" ------------------------------------------
" Если пользуемся менеджеом плагинов (требует git)
" Настраиваем расположение наших плагинов
"call plug#begin('~/.vim/plugged')

" Настройки и перечисления плагинов
" The NERD Tree
"Plug 'scrooloose/nerdtree'
" mediawiki.vim
"Plug 'chikamichi/mediawiki.vim'

" Инициализация плагинов
"call plug#end()
" ------------------------------------------
" Пользуем плагины без менеджера
set runtimepath+=~/.vim/plugged/mediawiki.vim
set runtimepath+=~/.vim/plugged/nerdtree
" ------------------------------------------

" Дополнительные настройки для плагинов
" Ctrl+n - запуск NERD Tree
map <silent> <C-n> :NERDTreeFocus<CR>

" Включение и отключение номера строк на <F2>
nnoremap <F2> :set relativenumber!<CR>
"
" Включение и отключение режима вставки на <F3> (решение проблемы сбивания отступов при вставке)
nnoremap <F3> :set paste!<CR>

" Подцветка синтаксиса
syntax on

" Подцветка найденных выражений - сразу всех
set hlsearch

" Подцветка во время набора в поиске
set incsearch

" Установить авто-отступ
set autoindent

" Включаем "умные" отступы ( например, авто-отступ после {)
set smartindent

" Преобразование Tab-а в пробелы - не пользую, но вдруг
"set expandtab

" Величина отступов по умолчанию - тоже не пользую
"set shiftwidth=4
"set softtabstop=4
"set tabstop=4

" Включаем определение типов файлов
if has("autocmd")
	filetype on
	filetype plugin on
	filetype indent on
	autocmd FileType php setlocal ts=4 sts=4 sw=4 noexpandtab
	autocmd FileType python setlocal ts=4 sts=4 sw=4 noexpandtab
	autocmd FileType javascript setlocal ts=4 sts=4 sw=4 noexpandtab

	autocmd FileType html setlocal ts=2 sts=2 sw=2 noexpandtab
	autocmd FileType xhtml setlocal ts=2 sts=2 sw=2 noexpandtab
	autocmd FileType xml setlocal ts=2 sts=2 sw=2 noexpandtab
	autocmd FileType css setlocal ts=2 sts=2 sw=2 noexpandtab

	autocmd FileType vim setlocal ts=4 sts=4 sw=4 noexpandtab
	autocmd FileType apache setlocal ts=2 sts=2 sw=2 noexpandtab
	autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

	autocmd BufNewFile,BufRead *.tmpl,*.tpl setfiletype html
endif

" Для указанных типов файлов отключает замену табов пробелами и меняет ширину отступа
au FileType crontab,fstab,make set noexpandtab tabstop=8 shiftwidth=8 softtabstop=8

" Показывать табуляцию (tab) в виде стрелок и пробелы на конце строк (trail)
set listchars=tab:▸▹,trail:•
set list

" Показывать перенос длинной строки
set showbreak=↳\

" Вкл. поддержку мыши - не понималЖ помогает это или нет
"if has('mouse')
"    set mouse=a
"    set mousemodel=popup
"endif

" Простое включение мыши
set mouse=a

" Внешний отступ от курсора; Marging.
set scrolloff=7

" Цветовая схема - мне такая нравится, она дефолтна
colorscheme desert

" Номера строк - ниже более красивое решение
"set number

" Относительные номера строк - ниже более красивое решение
set relativenumber

" Как только теряем фокус - номера обычные, как только в фокусе - номера относительные
" у меня пока не работает :(
":au FocusLost * :set number
":au FocusGained * :set relativenumber

" Копирование в системный буфер
"set clipboard=unnamedplus

" Выделить все по Ctrl+A
"map <C-a> <esc>ggVG<CR>

" Отключаем стрелки для передвижения
"nnoremap <up>    <nop>
"nnoremap <down>  <nop>
"nnoremap <left>  <nop>
"nnoremap <right> <nop>
"inoremap <up>    <nop>
"inoremap <down>  <nop>
"inoremap <left>  <nop>
"inoremap <right> <nop>

"" Формат строки состояния
" fileformat - формат файла (unix, dos); fileencoding - кодировка файла;
" " encoding - кодировка терминала; TYPE - тип файла, затем коды символа под
" курсором;
" " позиция курсора (строка, символ в строке); процент прочитанного в файле;
" " кол-во строк в файле;
set statusline=%F%m%r%h%w\ [FF,FE,TE=%{&fileformat},%{&fileencoding},%{&encoding}\]\ [TYPE=%Y]\ [ASCII=\%03.3b]\ [HEX=\%02.2B]\ [POS=%04l,%04v][%p%%]\ [LEN=%L]

"Изменяет шрифт строки статуса (делает его не жирным)
hi StatusLine gui=reverse cterm=reverse
set laststatus=2 " всегда показывать строку состояния


