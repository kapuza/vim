# Vim configs

## Plugins
* mediawiki
* nerdtree

## Unpack
~~~
cd ~; \
wget -O vim.tgz "https://gitlab.com/kapuza/vim/repository/master/archive.tar.gz"; \
tar -xf vim.tgz; \
mv ./vim-master-*/.vim* ./; \
rm -rf ./vim*;
~~~

## Map keys
* **Ctrl+n** - NerdTree
* **F2** - :set relativenumber!
* **F3** - :set paste!